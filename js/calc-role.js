$("document").ready(function(){
var link = window.location.hash;

    $("#quiz-button").click(function(){
      var lanes = [0, 0, 0, 0, 0];

      var q1 = $('input[name=question1]:checked', '#quizForm');
      var q2 = $('input[name=question2]:checked', '#quizForm');
      var q3 = $('input[name=question3]:checked', '#quizForm');
      var q4 = $('input[name=question4]:checked', '#quizForm');
      var q5 = $('input[name=question5]:checked', '#quizForm');
      var q6 = $('input[name=question6]:checked', '#quizForm');

      if(q1.val() == "top"){
          lanes[0]++;
      } else if(q1.val() == "jg"){
          lanes[1]++;
      } else if(q1.val() == "mid"){
          lanes[2]++;
      } else if(q1.val() == "adc"){
          lanes[3]++;
      } else if(q1.val() == "supp"){
          lanes[4]++;
      }

      if(q2.val() == "top"){
          lanes[0]++;
      } else if(q2.val() == "jg"){
          lanes[1]++;
      } else if(q2.val() == "mid"){
          lanes[2]++;
      } else if(q2.val() == "adc"){
          lanes[3]++;
      } else if(q2.val() == "supp"){
          lanes[4]++;
      }

      if(q3.val() == "top"){
          lanes[0]++;
      } else if(q3.val() == "jg"){
          lanes[1]++;
      } else if(q3.val() == "mid"){
          lanes[2]++;
      } else if(q3.val() == "adc"){
          lanes[3]++;
      } else if(q3.val() == "supp"){
          lanes[4]++;
      }

      if(q4.val() == "top"){
          lanes[0]++;
      } else if(q4.val() == "jg"){
          lanes[1]++;
      } else if(q4.val() == "mid"){
          lanes[2]++;
      } else if(q4.val() == "adc"){
          lanes[3]++;
      } else if(q4.val() == "supp"){
          lanes[4]++;
      }

      if(q5.val() == "top"){
          lanes[0]++;
      } else if(q5.val() == "jg"){
          lanes[1]++;
      } else if(q5.val() == "mid"){
          lanes[2]++;
      } else if(q5.val() == "adc"){
          lanes[3]++;
      } else if(q5.val() == "supp"){
          lanes[4]++;
      }

      if(q6.val() == "top"){
          lanes[0]++;
      } else if(q6.val() == "jg"){
          lanes[1]++;
      } else if(q6.val() == "mid"){
          lanes[2]++;
      } else if(q6.val() == "adc"){
          lanes[3]++;
      } else if(q6.val() == "supp"){
          lanes[4]++;
      }

      if (lanes[0] > lanes[1] && lanes[0] > lanes[2] && lanes[0] > lanes[3] && lanes[0] > lanes[4]){
        alert("Top!");
      } else if (lanes[1] > lanes[0] && lanes[1] > lanes[2] && lanes[1] > lanes[3] && lanes[1] > lanes[4]){
        alert("Jungler!");
      } else if (lanes[2] > lanes[0] && lanes[2] > lanes[1] && lanes[2] > lanes[3] && lanes[2] > lanes[4]){
        alert("Mid!");
      } else if (lanes[3] > lanes[0] && lanes[3] > lanes[2] && lanes[3] > lanes[1] && lanes[3] > lanes[4]){
        alert("Ad Carry!");
      } else if (lanes[4] > lanes[0] && lanes[4] > lanes[2] && lanes[4] > lanes[3] && lanes[4] > lanes[1]){
        alert("Support!");
      }

    });

})
