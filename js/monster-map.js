$("document").ready(function(){

var interval;

    function countdown(element, minutes, seconds) {
        interval = setInterval(function() {
            var el = document.getElementById(element);
            if(seconds == 0) {
                if(minutes == 0) {
                    clearInterval(interval);
                    return;
                } else {
                    minutes--;
                    seconds = 60;
                }
            }
            if(minutes > 0) {
                var minute_text = minutes + (minutes > 1 ? ' m' : ' m');
            } else {
                var minute_text = '';
            }
            var second_text = seconds > 1 ? 's' : 's';
            el.innerHTML = minute_text + ' ' + seconds + ' ' + second_text;
            seconds--;
        }, 1000);
    }

    $("#baron-cd").click(function(){
      $("#baron").attr('src',"../img/baron-dead.png");
      countdown('baron-cd',6,59);
    });
    $("#dragon-cd").click(function(){
      $("#dragon").attr('src',"../img/drag-dead.png");
      countdown('dragon-cd', 5, 59);
    });
    $("#birda-cd").click(function(){
      $("#birdsa").attr('src',"../img/birds-dead.png");
      countdown('birda-cd', 1, 40);
    });
    $("#birdb-cd").click(function(){
      $("#birdsb").attr('src',"../img/birds-dead.png");
      countdown('birdb-cd', 1, 40);
    });
    $("#bluea-cd").click(function(){
      $("#bluea").attr('src',"../img/blue-dead.png");
      countdown('bluea-cd', 4, 59);
    });
    $("#blueb-cd").click(function(){
      $("#blueb").attr('src',"../img/blue-dead.png");
      countdown('blueb-cd', 4, 59);
    });
    $("#froga-cd").click(function(){
      $("#froga").attr('src',"../img/frog-dead.png");
      countdown('froga-cd', 1, 40);
    });
    $("#frogb-cd").click(function(){
      $("#frogb").attr('src',"../img/frog-dead.png");
      countdown('frogb-cd', 1, 40);
    });
    $("#reda-cd").click(function(){
      $("#reda").attr('src',"../img/red-dead.png");
      countdown('reda-cd', 4, 59);
    });
    $("#redb-cd").click(function(){
      $("#redb").attr('src',"../img/red-dead.png");
      countdown('redb-cd', 4, 59);
    });
    $("#scuttlera-cd").click(function(){
      $("#scuttlera").attr('src',"../img/Rift_Scuttler-dead.png");
      countdown('scuttlera-cd', 2, 59);
    });
    $("#scuttlerb-cd").click(function(){
      $("#scuttlerb").attr('src',"../img/Rift_Scuttler-dead.png");
      countdown('scuttlerb-cd', 2, 59);
    });
    $("#rocka-cd").click(function(){
      $("#rocka").attr('src',"../img/rock-dead.png");
      countdown('rocka-cd', 1, 40);
    });
    $("#rockb-cd").click(function(){
      $("#rockb").attr('src',"../img/rock-dead.png");
      countdown('rockb-cd', 1, 40);
    });
    $("#wolfa-cd").click(function(){
      $("#wolfa").attr('src',"../img/wolf-dead.png");
      countdown('wolfa-cd', 1, 40);
    });
    $("#wolfb-cd").click(function(){
      $("#wolfb").attr('src',"../img/wolf-dead.png");
      countdown('wolfb-cd', 1, 40);
    });

})
