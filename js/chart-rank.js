window.onload = function () {
    var chart = new CanvasJS.Chart("chartContainer",
    {
      theme:"theme2",
      backgroundColor: "transparent",
      color: "white",
      animationEnabled: true,
      axisY :{
        includeZero: false,
        valueFormatString: "#,,."
      },
      toolTip: {
        shared: "true"
      },
      data: [
      {
        type: "spline",
        showInLegend: true,
        name: "Season 7",
        dataPoints: [
          {label: "Bronze", y: 260},
          {label: "Silver", y: 200},
          {label: "Gold", y: 180},
          {label: "Platinum", y: 100},
          {label: "Diamond", y: 60},

        ]
      },
      {
        type: "spline",
        showInLegend: true,
        name: "Season 6",
        dataPoints: [
        {label: "Bronze", y: 150},
        {label: "Silver", y: 180},
        {label: "Gold", y: 100},
        {label: "Platinum", y: 60},
        {label: "Diamond", y: 10},

        ]
      }
      ,{
        type: "spline",
        showInLegend: true,
        name: "Season 5",
        dataPoints: [
        {label: "Bronze", y: 80},
        {label: "Silver", y: 70},
        {label: "Gold", y: 50},
        {label: "Platinum", y: 10},
        {label: "Diamond", y: 5},

        ]
      }


      ],
      legend:{
        cursor:"pointer",
        itemclick : function(e) {
          if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible ){
          	e.dataSeries.visible = false;
          }
          else {
            e.dataSeries.visible = true;
          }
          chart.render();
        }

      },
    });

chart.render();
}
