var title_baron = "Baron Nashor";
var para_baron = "Baron Nashor is the most powerful neutral monster on Summoner's Rift in League of Legends. Killing Baron Nashor grants Hand of Baron to all living teammates for 210 seconds (242 with Runic Affinity) The buff gives bonus attack damage, bonus ability power, Empowered Empowered Recall, and an aura that greatly increases the power of nearby minions.";

var title_bird = "Crimson Raptor";
var para_bird = "The Crimson Raptor is a large and ranged monster found on the Summoner's Rift, along with five Raptor Raptors.\n\nA Raptor is a small monster on Summoner's Rift. Five of them can be found with each Crimson Raptor Crimson Raptor.\n\nSmite cannot be used on small monsters.";

var title_blue = "Blue Sentinel";
var para_blue = "The Blue Sentinel is a neutral monster on Summoner's Rift. It has negative magic resistance.\n\nKill the Blue Sentinel to receive the Crest of Insight, a buff which grants increased mana and energy regeneration as well as +10% cooldown reduction.";

var title_dragon = "Dragon";
var para_dragon = "Dragon is the second most powerful neutral monster on Summoner's Rift in League of Legends. Slaying Dragon grants the team one of Dragon Slayer buffs. Dragon's level ranges from 6 to 18.\n\nThere are 5 variations of Dragon: Cloud Drake, Infernal Drake, Ocean Drake, Mountain Drake and Elder Drake.";

var title_frog = "Gromp";
var para_frog = "Gromp is a large toad monster present on the visually upgraded version of Summoner's Rift. It is nearly identical to the now removed Wight Wight.\n\nGromp gains 100% attack speed at the beginning of the fight, decaying over 5 attacks.";

var title_red = "Red Brambleback";
var para_red = "The Red Brambleback is a neutral monster on Summoner's Rift. It has negative armor.\n\nKill the Red Brambleback to receive the Crest of Cinders, a buff which grants health regeneration and causes your basic attacks to slow the enemy while also dealing true damage over time.";

var title_rock = "Ancient Krug";
var para_rock = "The Ancient Krug is a large monster found on the Summoner's Rift along with a smaller Krug Krug. The Ancient Krug spawns two Krugs when killed, which will each spawn two Mini Krugs when killed.\n\nIf the Ancient Krug is killed and the Krug(s) or Mini Krug(s) are spared, they despawn after a few seconds (e.g. Summoner 1 is counter-jungling and kills the Ancient Krug. The Ancient Krug splits into the two Krugs. The summoner kills the two Krugs which leaves behind four Mini Krugs. If the Summoner then retreats, the four Mini Krugs will despawn)";

var title_scuttler = "Rift Scuttler";
var para_scuttler = "Rift Scuttler is a large neutral monster found on Summoner's Rift. There are two Rift Scuttlers on the map, one found in each half of the river spanning the middle of the terrain.\n\nRift Scuttler is unique amongst all monsters in the game for two reasons: she will not retaliate against an attacker, instead attempting to flee directly away from them, and she moves along a fixed path in the river on the part of the map where she spawns. This makes Rift Scuttler the only non-hostile and autonomously moving monster on the map.";

var title_wolf = "Greater Murk Wolf";
var para_wolf = "The Greater Murk Wolf is large monster on Summoner's Rift found with two Murk Wolf Murk Wolves.\n\nThe Murk Wolf is a small monster on Summoner's Rift. Two of them could be found accompanying a Greater Murk Wolf Greater Murk Wolf.";

var img = "../img/better-img-quality/";

$("document").ready(function(){
  var link = window.location.hash;

  verifyLink();
  function verifyLink(){
    var monster_selected;
    var img_extension = ".jpg";

    if (link == ""){
      return false;
    } else if (link == "#baron"){
      monster_selected = "baron";
    } else if (link == "#raptor"){
      monster_selected = "bird";
    } else if (link == "#dragon"){
      monster_selected = "dragon";
    } else if (link == "#blue"){
      monster_selected = "blue";
    } else if (link == "#gromp"){
      monster_selected = "frog";
    } else if (link == "#red"){
      monster_selected = "red";
    }  else if (link == "#krug"){
      monster_selected = "rock";
    }  else if (link == "#scuttler"){
      monster_selected = "scuttler";
    }  else if (link == "#wolf"){
      monster_selected = "wolf";
    }

    var img_src = img + monster_selected + img_extension;
    var title_src = window["title_" + monster_selected];
    var para_src = window["para_"  + monster_selected];

    document.getElementById('monsters-img').src = img_src;
    document.getElementById('monsters-name').innerHTML = title_src;
    document.getElementById('monsters-text').innerHTML = para_src;
  }

})
